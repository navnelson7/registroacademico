<?php
/*
|---------------------------------------------|
|      Rutas Web para alumnos de basica       |
|---------------------------------------------|
*/
Auth::routes();

Route::get('/', 'UsuarioController@MostrarLogin')->name('home');
Route::get('/CBIS', 'HomeController@index');
/*
|-------------------------------------|
|rutas para el apartado de Seleccion. |
|-------------------------------------|
*/
Route::post('/CBIS/NotaMensualBasica', 'NotaMensualBasicaController@FormularioNotaMensualBasica')->name('home');
Route::post('/CBIS/NotaMensualBasica/GuardarNota', 'NotaMensualBasicaController@GuardarNotas')->name('home');
/*
|----------------------------------|
|rutas para el apartado de buscar. |
|----------------------------------|
*/
Route::get('/CBIS/Buscar', 'NotaMensualBasicaController@FormularioBuscar')->name('home');
Route::post('/CBIS/Buscar/AlumnoNotas', 'NotaMensualBasicaController@ResultadoNotas')->name('home');
Route::get('/CBIS/Buscar/AlumnoNotas/{id}/editar', 'NotaMensualBasicaController@formularioeditardoNotas')->name('home');
Route::post('/CBIS/Buscar/AlumnoNotas/{id}/editar', 'NotaMensualBasicaController@modificarNotas')->name('home');
Route::post('/CBIS/Buscar/AlumnoNotas/{id}/EliminarAlumnoBasica', 'NotaMensualBasicaController@EliminarAlumnoBasica')->name('home');
/*
================================================================================
================================================================================

|---------------------------------------------------|
|      Rutas Web para alumnos de bachillerato       |
|---------------------------------------------------|
*/

Route::get('/CBIS/SeleccionBachillerato', 'SeleccionBachilleratoController@SeleccionBachiller')->name('home');

//inserción de notas
Route::post('/CBIS/IngresarNotas', 'NotasBachilleratoController@FormularioNotas')->name('home');
Route::post('/CBIS/IngresarNotas/GuardarNotas', 'NotasBachilleratoController@GuardarNotasBach')->name('home');

//buscar alumno
Route::get('/CBIS/BuscarAlumno/Bachillerato', 'NotasBachilleratoController@FormularioBuscarAlumnoBach')->name('home');
Route::post('/CBIS/BuscarAlumno/Bachillerato/Notas', 'NotasBachilleratoController@ResultadoNotasBach')->name('home');

//editar alumno
Route::get('/CBIS/Bachillerato/{id}/EditarNotas', 'NotasBachilleratoController@FormularioEditarNotasBach')->name('home');
Route::post('/CBIS/Bachillerato/{id}/EditarNotas', 'NotasBachilleratoController@ActualizarNotasBach')->name('home');

// eliminar
Route::post('/CBIS/Bachillerato/{id}/Eliminar', 'NotasBachilleratoController@EliminarNotasBach')->name('home');


/*
================================================================================
================================================================================


|-----------------------------------------------------|
|       rutas para el apartado de Nuevo Alumno.       |
|-----------------------------------------------------|
*/
 Route::get('/CBIS/NuevoAlumno', 'alumnocontroller@FormularioNuevoAlumno')->name('home');
 Route::post('/CBIS/NuevoAlumno/GuardarAlumno', 'alumnocontroller@GuardarAlumno')->name('home');

/*
|-----------------------------------------------------|
|       rutas para el apartado de matricula.          |
|-----------------------------------------------------|
*/
//mostrara lista de grados
Route::get('/CBIS/matricula', 'GradosController@ListaGrados')->name('home');
//listar alumnos de un grado seleccionado
Route::post('/CBIS/matricula/ListaAlumno', 'MatriculaController@ListarAlumnos')->name('home');

Route::get('/CBIS/matricula/ListaAlumno/{id}/matricular', 'MatriculaController@formulariomatricular')->name('home');
Route::post('/CBIS/matricula/ListaAlumno/matricularalumno', 'MatriculaController@GuardarMatricula')->name('home');




















///espacio
