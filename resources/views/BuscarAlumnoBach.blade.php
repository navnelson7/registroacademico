<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Ingresar Notas - Bachillerato') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
</head>
  <body class="fondo-grey-light"  >

      @include('MenuBachillerato')
      <br>
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-md-10">
                  <div class="card border-primary">
                    <div>
                      <h4 class="card-header text-center text-white font-weight-bold fondoazul">Buscar Alumno</h4>
                    </div>
                    <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif
                      <form  action="{{ url('/CBIS/BuscarAlumno/Bachillerato/Notas') }}" method="post">
                        @csrf
                        <div class="form-group row">
                          <label for="" class="col-sm-3 col-form-label">Identificación del Alumno:</label>
                          <div class="col-sm-9 my-1">
                            <input name="idalumno" type="text" class="form-control" placeholder="Igrese la Identificación" required>
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label" for="inlineFormCustomSelect">Seleccione el Periodo:</label>
                          <div class="col-sm-9 my-1">
                             <select name="periodoseleccionado" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                               @foreach ($periodos as $key => $value)
                                 <option value="{{ $key }}">{{ $value }}</option>
                               @endforeach
                             </select>
                           </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label" for="inlineFormCustomSelect">Seleccione el Grado:</label>
                          <div class="col-md-9 my-1">
                            <select name="gradoseleccionado" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                @foreach ($grados as $grado)
                                   <option value="{{ $grado->idgrado }}">{{ $grado->grado }}</option>
                                 @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label" for="inlineFormCustomSelect">Seleccione el Año:</label>
                          <div class="col-md-9 my-1">
                            <select name="anioseleccionado" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                              @foreach ($valoresunicos as $nota)
                                @if ($nota->ano > 0)
                                  <option value="{{ $nota->ano }}">{{ $nota->ano }}</option>
                                @endif
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="form-group row justify-content-end">
                          <div class="col-sm-9 my-1">
                            <input type="submit" class=" btn btn-outline-primary col-sm-12 font-weight-bold"  name="" value="Seleccionar Alumno">
                          </div>
                        </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>

  </body>
</html>
