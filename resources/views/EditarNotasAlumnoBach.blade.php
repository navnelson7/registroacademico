<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Ingresar Notas - Bachillerato') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
</head>
  <body>
    @include('MenuBachillerato')
    <br>
        <form action="{{ url('/CBIS/Bachillerato/ '. $notas->id .' /EditarNotas') }}" method="post">
          @csrf
          <input type="hidden" name="idasignatura" value="{{ $notas->idasignatura }}">
          <input type="hidden" name="idgrado" value="{{ $notas->idgrado }}">    <div class="table-responsive ">
            <table class="table table-hover">
              <thead class="fondoazul text-center text-white font-weight-bold">
                <tr>
                  <th scope="col">IDalumno</th>
                  <th scope="col">Alumno</th>
                  <th scope="col">Periodo</th>
                  <th scope="col">Actividad1</th>
                  <th scope="col">Actividad2</th>
                  <th scope="col">Actividad3</th>
                  <th scope="col">Actividad4</th>
                  <th scope="col">Actividad5</th>
                  <th scope="col">Examen</th>
                  <th scope="col">ExamenP</th>
                  </tr>
                </thead>
              <tbody>
                <tr>
                  <th scope="row">
                    {{ $notas->idalumno}}
                  </th>
                  <td>
                    {{ $nombrecompleto }}
                  </td>
                  <td>
                    <div class="form-group ">
                      <div class="col-xs-2">
                        <input disabled type="text" class="form-control" name="periodo" id="per" value="{{ $notas->periodo }}" onblur="mayus(this.value, this.id)" >
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group ">
                      <div class="col-xs-2">
                        <input type="number" class="form-control" name="a1" value="{{ $notas->A1 }}">
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group ">
                      <div class="col-xs-2">
                        <input type="number" class="form-control" name="a2" value="{{ $notas->A2 }}">
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group ">
                      <div class="col-xs-2">
                        <input type="number" class="form-control" name="a3" value="{{ $notas->A3 }}">
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group ">
                      <div class="col-xs-2">
                        <input type="number" class="form-control" name="a4" value="{{ $notas->A4 }}">
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group ">
                      <div class="col-xs-2">
                        <input type="number" class="form-control" name="a5" value="{{ $notas->A5 }}"><br>
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group ">
                      <div class="col-xs-2">
                        <input type="number " class="form-control" name="examen" value="{{ $notas->EXAMEN }}"><br>
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group ">
                      <div class="col-xs-2">
                        <input type="number" class="form-control" name="examenp" value="{{ $notas->EXAMENP }}"><br>
                      </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <br>
          <div class="form-group row justify-content-center">
            <div class="col-sm-9 my-1">
              <input type="submit" class=" btn btn-outline-primary col-sm-12 font-weight-bold" value="Guardar notas">
            </div>
          </div>
          <br>
      </form>
  
      <script type="text/javascript">
        function mayus(obj, id){
          obj = obj.toUpperCase();
          document.getElementById(id).value = obj
        }
      </script>
  </body>
</html>
