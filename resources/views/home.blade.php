@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card border-primary">
              <div>
                <h4 class="card-header text-center text-white font-weight-bold fondoazul">Selección</h4>
              </div>
              <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form  action="{{ url('/CBIS/NotaMensualBasica') }}" method="post">
                  @csrf
                  <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">Identificación del Alumno :</label>
                    <div class="col-sm-9 my-1">
                      <input name="idalumno" type="text" class="form-control" placeholder="Igrese la Identificación" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="inlineFormCustomSelect">Seleccione a la Asignatura : </label>
                    <div class="col-sm-9 my-1">
                       <select name="asignaturaseleccionada" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                         @foreach ($asignaturas as $asignatura)
                            <option value="{{ $asignatura->idasignatura }}">{{ $asignatura->asignatura }}</option>
                          @endforeach
                       </select>
                     </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="inlineFormCustomSelect">Seleccione el Mes :</label>
                    <div class="col-md-9 my-1">
                      <select name="messeleccionado" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                          @foreach ($meses as $mes)
                             <option value="{{ $mes->idmes }}">{{ $mes->mes }}</option>
                           @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row justify-content-end">
                    <div class="col-sm-9 my-1">
                      <input type="submit" class=" btn btn-outline-primary col-sm-12 font-weight-bold"  name="" value="Seleccionar Alumno">
                    </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
