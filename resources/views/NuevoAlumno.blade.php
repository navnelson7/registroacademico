<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Scripts -->
      <script src="{{ asset('js/app.js') }}" defer></script>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
  </head>
  <body class="fondo-grey-light">
      <div id="app  m-0 p-" >
        @include('menu')
        <br>
        <div class="container col-md-10">
          <div class="card border-primary ">
            <div class="card-header text-center text-white font-weight-bold fondoazul">Agregar Nuevo Alumno</div>
            <div class="card-body text-primary">

              <form action="{{ url('/CBIS/NuevoAlumno/GuardarAlumno') }}" method="post">
                @csrf
                <div class="row">
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Id Alumno :</label>
                    <div class="col-sm-9   my-1">
                      <input type="text" name="idalumno" class="form-control" placeholder="Igrese idalumno" required>
                    </div>
                  </div>
                  <div class="form-group row col-md-6 ">
                    <label for="" class="col-sm-3 col-form-label">Nombres :</label>
                    <div class="col-sm-9 my-1">
                      <input type="text" name="nombres" class="form-control" placeholder="Nombres" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Apellidos :</label>
                    <div class="col-sm-9 my-1">
                      <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" required>
                    </div>
                  </div>
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Edad :</label>
                    <div class="col-sm-9 my-1">
                      <input type="number" name="edad" class="form-control" placeholder="Edad del Alumno" required min="4" max="25">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group row col-md-6 ">
                    <label for="" class="col-sm-5 col-form-label">Fecha de Nacimiento:</label>
                    <div class="col-sm-7 my-1">
                      <input type="text" name="fechadenacimiento" class="form-control" placeholder="Fecha de Nacimiento" required>
                    </div>
                  </div>
                  <div class="form-group row col-md-6 ">
                    <label for="" class="col-sm-3 col-form-label">Año :</label>
                    <div class="col-sm-9 my-1">
                      <input type="text" name="anio" class="form-control" placeholder="Igrese año de ingrso" required>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Sexo :</label>
                    <select name="sexo" class="custom-select col-sm-9 my-1  " id="inlineFormCustomSelect" required>
                         <option value="m">Masculino</option>
                         <option value="f">Femenino</option>
                         <option value="o">Otro</option>
                    </select>
                  </div>
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Telefono :</label>
                    <div class="col-sm-9 my-1">
                      <input type="number" name="telefono" class="form-control" placeholder="Telefono" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Padre :</label>
                    <div class="col-sm-9 my-1">
                      <input type="text" name="padre" class="form-control" placeholder="Padre" required>
                    </div>
                  </div>
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Madre :</label>
                    <div class="col-sm-9 my-1">
                      <input type="text" name="madre" class="form-control" placeholder="Madre" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group row col-md-12 m-1 p-1">
                    <label for="" class="col-sm-2 col-form-label m-1 p-2">Direccion:  </label>
                    <div class="col-sm-12 col-md-9 m-1 p-1">
                      <input type="text" name="direccion" class="form-control" placeholder="Direccion" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Encargado :</label>
                    <div class="col-sm-9 my-1">
                      <input type="text" name="encargado" class="form-control" placeholder="Encargado" required>
                    </div>
                  </div>
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Parentesco:</label>
                    <select name="parentesco" class="custom-select col-sm-9 my-1  " id="inlineFormCustomSelect" required>
                         <option value="p">Padre</option>
                         <option value="m">Madre</option>
                         <option value="a">Abuelo/a</option>
                         <option value="t">Tio/a</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">Estado :</label>
                    <select name="estado" class="custom-select col-sm-9 my-1  " id="inlineFormCustomSelect" required>
                         <option value="a">Activo</option>
                         <option value="i">Inactivo </option>
                    </select>
                  </div>
                  <div class="form-group row col-md-6">
                    <label for="" class="col-sm-3 col-form-label">IdGrado:</label>
                    <div class="col-sm-9 my-1">
                      <input type="text" name="idgrado" class="form-control" placeholder="Idgrado" required>
                    </div>
                  </div>
                </div>
                <div class="">
                  <div class="form-group row justify-content-center">
                    <div class="col-sm-10 my-1">
                      <input type="submit" class=" btn btn-outline-primary col-sm-12 font-weight-bold" value="Seleccionar Alumno" required>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </body>
</html>
