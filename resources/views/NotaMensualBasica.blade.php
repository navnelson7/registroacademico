<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Scripts -->
      <script src="{{ asset('js/app.js') }}" defer></script>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
  </head>
  <body class="fondo-grey-light">
    @include('menu')
    <br><br>
    <form class="container bg-white" action="{{ url('/CBIS/NotaMensualBasica/GuardarNota') }}" method="post">
      @csrf
      <input type="hidden" name="idmes" value="{{ $idmes }}">
      <input type="hidden" name="idano" value="{{ $idano }}">
      <input type="hidden" name="idasignatura" value="{{ $idasignatura }}">
      <input type="hidden" name="idgrado" value="{{ $idgrado }}">
      <input type="hidden" name="idalumno" value="{{ $idalumno }} ">
      <div class="table-responsive">
        <table class="table table-hover">
        <thead class="text-center text-white font-weight-bold fondoazul">
          <tr>
            <th scope="col">#Idalumno</th>
            <th class="">Alumno</th>
            <th class="">Periodo</th>
            <th class="">A1</th>
            <th class="">A2</th>
            <th class="">A3</th>
            <th class="">A4</th>
            <th scope="col">Promedio</th>
            <th scope="col">P60</th>
            <th scope="col">Examen</th>
            <th scope="col">P40</th>
            <th scope="col">PFinal</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{{ $idalumno }}</td>
            <td class="col-md-2">{{$nombrecompleto}}</td>
            <td>
              <div class="form-group row">
                <div class="col-xs-2">
                  <input class="form-control" name="periodo"  type="text" required>
                </div>
              </div>
            </td>
            <td>
              <div class="form-group row m-1 ">
                <div class="col-xs-1">
                  <input class="form-control" type="text" name="a1" value="" required>
                </div>
              </div>
            </td>
            <td >
              <div class="form-group row m-1 ">
                <div class="col-xs-1">
                  <input class="form-control" type="text" name="a2" value="" required>
                </div>
              </div>
            </td>
            <td>
              <div class="form-group row m-1 ">
                <div class="col-xs-1">
                  <input class="form-control" type="text" name="a3" value="" required>
                </div>
              </div>
            </td>
            <td>
              <div class="form-group  row m-1 ">
                <div class="col-xs-1">
                  <input class="form-control" type="text" name="a4" value="" required>
                </div>
              </div>
            </td>
            <td>
              <div class="form-group  row m-1 ">
                <div class="col-xs-1">
                  <input class="form-control" type="text" name="pro" value="" required>
                </div>
              </div>
            </td>
            <td>
              <div class="form-group  row m-1 ">
                <div class="col-xs-1">
                  <input class="form-control" type="text" name="p60" value="" required>
                </div>
              </div>
            </td>
            <td>
              <div class="form-group  row m-1 ">
                <div class="col-xs-1">
                  <input class="form-control" type="text" name="exa" value="" required>
                </div>
              </div>
            </td>
            <td>
              <div class="form-group  row m-1 ">
                <div class="col-xs-1">
                  <input class="form-control" type="text" name="p40" value="" required>
                </div>
              </div>
            </td>
            <td>
              <div class="form-group  row m-1 ">
                <div class="col-xs-1">
                  <input class="form-control" type="text" name="pf" value="" required>
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
      </div>
      <div class="form-group row justify-content-center">
        <div class="col-sm-9 my-1">
          <input type="submit" class=" btn btn-outline-primary col-sm-12 font-weight-bold"  name="" value="Guardar Notas
          ">
        </div>
      </div>
    </form>
    </body>
</html>
