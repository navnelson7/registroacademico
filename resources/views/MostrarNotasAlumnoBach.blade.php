<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Ingresar Notas - Bachillerato') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
</head>
  <body class="fondo-grey-light">
    @include('MenuBachillerato')
      <br>
      <div class="container">
        <div class="col-md-12">
          <div>
            <h4 class="card-header text-center text-blue-darken font-weight-bold bg-white">
              Alumno: {{ $nombrecompleto }}
            </h4>
          </div>
          <table class="table table-hover bg-white">
            <thead class="text-center text-white fondoazul">
              <tr>
                <th scope="col">IDalumno</th>
                <th scope="col">Asignatura</th>
                <th scope="col">A1</th>
                <th scope="col">A2</th>
                <th scope="col">A3</th>
                <th scope="col">A4</th>
                <th scope="col">A5</th>
                <th scope="col">P50</th>
                <th scope="col">Examen</th>
                <th scope="col">P20</th>
                <th scope="col">ExamenP</th>
                <th scope="col">P30</th>
                <th scope="col">PF</th>
                <th scope="col" colspan="2">Opciones</th>
              </tr>
            </thead>
            <tbody class="text-center">
            @forelse ($notas as $nota)
              @if ($idalumno1 == $nota->idalumno AND $periodo == $nota->periodo AND $idgrado == $nota->idgrado AND $anio == $nota->ano)
                <tr>
                  <td scope="col" class="font-weight-bold" >{{ $nota->idalumno }}</td>
                  <td>
                    @foreach($asignaturas as $asignatura)
                      @if($nota->idasignatura == $asignatura->idasignatura)
                        {{ $asignatura->asignatura }}
                      @endif
                    @endforeach
                  </td>
                  <td>{{ $nota->A1 }}</td>
                  <td>{{ $nota->A2 }}</td>
                  <td>{{ $nota->A3 }}</td>
                  <td>{{ $nota->A4 }}</td>
                  <td>{{ $nota->A5 }}</td>
                  <td>{{ $nota->P50 }}</td>
                  <td>{{ $nota->EXAMEN }}</td>
                  <td>{{ $nota->P20 }}</td>
                  <td>{{ $nota->EXAMENP }}</td>
                  <td>{{ $nota->P30 }}</td>
                  <td>{{ $nota->PF }}</td>
                  <td><a href="{{ url('/CBIS/Bachillerato/'. $nota->id .'/EditarNotas') }}" class="btn btn-success btn-sm">Editar</a></td>
                  <td>
                    <form action="{{ url('/CBIS/Bachillerato/'. $nota->id .'/Eliminar') }}" method="post">
                      @csrf
                      <input type="submit" name="" value="Eliminar" onclick="return confirm('esta seguro ?')" class="btn btn-danger btn-sm">
                    </form>
                  </td>
                  </tr>
              @endif
            @empty
              <p>No existen registros con esos datos dados</p>
            @endforelse

            </tbody>
          </table>
        </div>
      </div>
  </body>
</html>
