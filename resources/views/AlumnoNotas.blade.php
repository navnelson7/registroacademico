<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Scripts -->
      <script src="{{ asset('js/app.js') }}" defer></script>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
  </head>
  <body class="fondo-grey-light">
    @include('menu')
    <br><br>
    <div class="container col-md-11">
      <table class="table table-hover bg-white">
        <thead class="fondoazul text-white">
          <tr>
            <th scope="col">IdRegistro</th>
            <th scope="col">Nombre Completo</th>
            <th scope="col">Asignatura</th>
            <th scope="col">A1</th>
            <th scope="col">A2</th>
            <th scope="col">A3</th>
            <th scope="col">A4</th>
            <th scope="col">P</th>
            <th scope="col">60%</th>
            <th scope="col">E</th>
            <th scope="col">40%</th>
            <th scope="col">PM</th>
            <th scope="col">AÑO</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($notamensualbasica as $nota)
            @if($nota->idalumno ==  $idalumno1 and $nota->idmes == $idmes and $nota->idano == $idano)
            <tr>
              <th class="fondoazul text-white">{{$nota->idalumno}}</th>
              <td>{{ $nombrecompleto }}</th>
              <td>
                @foreach($asignaturasbasica as $asignaturas)
                  @if($nota->idasignatura == $asignaturas->idasignatura)
                    {{ $asignaturas->asignatura }}
                  @endif
                @endforeach
              </td>
              <td>{{ $nota->a1 }}</td>
              <td>{{ $nota->a2 }}</td>
              <td>{{ $nota->a3 }}</td>
              <td>{{ $nota->a4 }}</td>
              <td>{{ $nota->promedio }}</td>
              <td>{{ $nota->p60 }}</td>
              <td>{{ $nota->examen }}</td>
              <td>{{ $nota->p40 }}</td>
              <td>{{ $nota->promes }}</td>
              <td>{{ $nota->idano  }}</td>
              <td> <a class="btn bg-success text-white" href="{{ url('/CBIS/Buscar/AlumnoNotas/'.$nota->id.'/editar ')}}">editar </a> </td>
              <td>
                <form action="{{ url('/CBIS/Buscar/AlumnoNotas/'.$nota->id.'/EliminarAlumnoBasica' )}}" method="post">
                  @csrf
                  <input class="bg-danger btn text-white" type="submit" name="" value="Eliminar" onclick="return confirm('esta seguro ?')">
                </form>
              </td>

            </tr>
            @endif
           @endforeach
        </tbody>
      </table>
    </div>
  </body>
</html>
