<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Scripts -->
      <script src="{{ asset('js/app.js') }}" defer></script>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
  </head>
  <body>
    @include('menu')

    <div class="row p-1 m-1">
      <div class="col-md-11 ">
        <h3 class="text-center text-blue-darken font-weight-bold">Listado de alumnos</h3>
      </div>
      <h5> <span class="badge badge-info text-white text-center">Paso 2</span></h5>
    </div>
      <div class="container ">
        <table class="table table-hover text-center bg-white">
          <thead class=" text-white font-weight-bold fondoazul">
            <tr >
              <th scope="col">Codigo</th>
              <th scope="col">Nombres</th>
              <th scope="col">Apellidos</th>
              <th scope="col">Grados</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody >

              @foreach($matriculas as $matricula)
              <tr>
                <td >{{ $matricula->idalumno }}  </td>
                @foreach($alumnos as $alumno)
                  @if($matricula->idalumno == $alumno->idalumno)
                  <td name="nombres"> {{ $alumno->nombres }}</td>
                  <td>{{ $alumno->apellidos }}</td>
                  @endif
                @endforeach
                <td>{{ $matricula->idgrado }}</td>
                <td><a href="{{ url('/CBIS/matricula/ListaAlumno/'. $matricula->id .' /matricular') }}" class="btn btn-warning btn-sm">matricular</a></td>
              </tr>
              @endforeach

          </tbody>
        </table>
      </div>
  </body>
</html>
