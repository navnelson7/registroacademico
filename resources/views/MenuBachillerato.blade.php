<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a  class="nav-link p-0 m-0" href="{{ url('/CBIS')}}">
          <img src="{{ asset('imagenes/logotabernaculo.png') }}" width="40" height="40">
        </a>
          <a class="text-blue-darken font-weight-bold p-1 m-1">CBIS</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav">
              <li> <a class="nav-link" href="{{ url('/CBIS/SeleccionBachillerato') }}">Seleccion</a> </li>
              <li> <a class="nav-link" href="{{ url('/CBIS/BuscarAlumno/Bachillerato') }}">Buscar</a> </li>
              <li> <a class="nav-link" href="{{ url('/CBIS') }}">Registro Basica</a> </li>
              <li><a href="{{ url('/CBIS/NuevoAlumno')}}" class="nav-link">Nuevo Alumno</a></li>
              <li><a href="{{ url('/CBIS/matricula')}}" class="nav-link"> Matriculas </a></li>
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->idusuario }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
