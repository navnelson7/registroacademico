<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Scripts -->
      <script src="{{ asset('js/app.js') }}" defer></script>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
  </head>
  <body>
    @include('menu')
      <br>

      <form class="" action="{{ url('/CBIS/matricula/ListaAlumno/matricularalumno') }}" method="post">
        numtalonario<input type="text" name="talonario" value="">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">Codigo</th>
              <th scope="col">Nombres</th>
              <th scope="col">Apellidos</th>
              <th scope="col">Actual</th>
              <th scope="col">Matricular En :</th>
              <th scope="col">Año</th>
              <th scope="col">Estado</th>
              <th scope="col">Matriculo</th>

            </tr>
          </thead>
          <tbody>
              @csrf
              @foreach($matriculas as $matricula)
              <tr>
                <td >{{ $matricula->idalumno }} <input type="hidden" name="idalumno" value="{{ $matricula->idalumno }} "> </td>
                @foreach($alumnos as $alumno)
                  @if($matricula->idalumno == $alumno->idalumno)
                  <td name="nombres"> {{ $alumno->nombres }}</td>
                  <td>{{ $alumno->apellidos }}</td>
                  @endif
                @endforeach
                <td>{{ $matricula->idgrado }}</td>
                <td>
                  <div class="form-group row">
                    <div class="col-md-9 my-1">
                      <select name="gradoseleccionado" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                          @foreach ($grados as $grado)
                             <option value="{{ $grado->idgrado }}">{{ $grado->grado }}</option>
                           @endforeach
                      </select>
                    </div>
                  </div>
                </td>
                <td> <input type="text" name="anio" value="{{ $aniomayor->idano }}"> </td>

                <td>{{ $matricula->estado }} <input type="hidden" name="estado" value="{{ $matricula->estado }}"> </td>
                <td> {{ Auth::user()->idmae }} <input type="hidden" name="idmae" value="{{ Auth::user()->idmae }}"> </td>
              </tr>
              @endforeach
          </tbody>
        </table>
        <input type="submit" name="" value="Matricular">

      </form>
  </body>
</html>
