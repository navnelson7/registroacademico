<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Ingresar Notas - Bachillerato') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
</head>
  <body class="fondo-grey-light">
    @include('menu')
    <br>
    <div class="container col-md-7">
      <div class="card border-primary">
        <div class="card-header fondoazul text-center text-white font-weight-bold">
          <div class="row">
            <div class="col-md-10 ">
              <h4 class="">Lista de Grados para Matricular </h4>
            </div>
            <div class="pull-right">
              <h5> <span class="badge badge-info text-white text-center">Paso 1</span></h5>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="pull-left ">
            <h6> <span class="badge badge text-black m-2 p-2 fondo-grey-light">Info</span>Elejir un grado para iniciar el proceso de matricula</h6>
          </div>

          <form class="" action="{{ url('/CBIS/matricula/ListaAlumno') }}" method="post">
            @csrf
            <div class="form-group row">
              <label class="col-sm-3 col-form-label" for="inlineFormCustomSelect">Seleccione el Grado:</label>
              <div class="col-md-9 my-1">
                <select name="gradoseleccionado" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                    @foreach ($grados as $grado)
                       <option value="{{ $grado->idgrado }}">{{ $grado->grado }}</option>
                     @endforeach
                </select>
              </div>
            </div>
            <div class="pull-left">
              <h6><span class="badge badge text-black m-2 p-2 fondo-grey-light">Info</span>Elejir el año que la seccion ha culminado</h6>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label" for="inlineFormCustomSelect">Seleccione el año:</label>
              <div class="col-md-9 my-1">
                <select name="idanoseleccionado" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                    @foreach ($valoresunicos as $valores)
                       <option value="{{ $valores->idano }}">{{ $valores->idano }}</option>
                     @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row justify-content-end">
              <div class="col-sm-9 my-1">
                <input type="submit" class=" btn btn-outline-primary col-sm-12 font-weight-bold"  name="" value="Iniciar Matricula">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>


  </body>
</html>
