<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Scripts -->
      <script src="{{ asset('js/app.js') }}" defer></script>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
  </head>
  <body class="fondo-grey-light">
    @include('menu')
    <br><br>
    <div class="container col-md-8">
      <div class="card border-primary">
        <div class="card-header fondoazul text-center text-white font-weight-bold">
          Buscar Alumno de Basica
        </div>
        <div class="card-body">
          <form  action="{{ url('/CBIS/Buscar/AlumnoNotas') }}" method="post">
            @csrf
            <div class="form-group row">
              <label for="" class="col-sm-3 col-form-label">Id Alumno :</label>
              <div class="col-sm-8 my-1">
                <input name="idalumno" type="text" class="form-control" placeholder="A quien quieres buscar" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label" for="inlineFormCustomSelect">Seleccione el Mes :</label>
              <div class="col-md-8 my-1">
                <select name="messeleccionado" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                    @foreach ($meses as $mes)
                       <option value="{{ $mes->idmes }}">{{ $mes->mes }}</option>
                     @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row justify-content-end">
              <div class="col-sm-9 my-1">
                <input type="submit" class=" btn btn-outline-primary col-sm-12 font-weight-bold" value="Buscar">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
