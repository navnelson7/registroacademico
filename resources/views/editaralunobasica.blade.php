<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
      <!-- Scripts -->
      <script src="{{ asset('js/app.js') }}" defer></script>
      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('css/mi.css') }}" rel="stylesheet">
  </head>
  <body class="fondo-grey-light">
    @include('menu')
    <br><br>
    <form class="container bg-white " action="{{ url('/CBIS/Buscar/AlumnoNotas/'.$notamensualbasica->id.'/editar') }}" method="post">
      @csrf

      <table class="table table-hover">
        <thead class="fondoazul text-center text-white font-weight-bold">
          <tr>
            <th scope="col">IDalumno</th>
            <th>Actividad1</th>
            <th>Actividad2</th>
            <th>Actividad3</th>
            <th>Actividad4</th>
            <th>Promedio</th>
            <th>Examen</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">{{ $notamensualbasica->idalumno}}</th>
            <td>
              <div class="form-group ">
                <div class="col-xs-2">
                  <input type="text" class="form-control" name="a1" value="{{ $notamensualbasica->a1 }}">
                </div>
              </div>
            </td>
            <td>
              <div class="form-group ">
                <div class="col-xs-2">
                  <input type="text" class="form-control" name="a2" value="{{ $notamensualbasica->a2 }}">
                </div>
              </div>
            </td>
            <td>
              <div class="form-group">
                <div class="col-xs-2">
                  <input type="text" class="form-control" name="a3" value="{{ $notamensualbasica->a3 }}">
                </div>
              </div>
            </td>
            <td>
              <div class="form-group">
                <div class="col-xs-2">
                  <input type="text" class="form-control" name="a4" value="{{ $notamensualbasica->a4 }}">
                </div>
              </div>
            </td>
            <td>
              <div class="form-group">
                <div class="col-xs-2">
                  <input type="text" class="form-control" name="pro" value="{{ $notamensualbasica->promedio }}">                </div>
              </div>
            </td>
            <td>
              <div class="form-group ">
                <div class="col-xs-2">
                  <input type="text" class="form-control" name="exa" value="{{ $notamensualbasica->examen }}">                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
      <div class="form-group row justify-content-center">
        <div class="col-sm-9 my-1">
          <input type="submit" class=" btn btn-outline-primary col-sm-12 font-weight-bold"  name="" value="Terminar Edicion">
        </div>
      </div>
    </form>
  </body>
</html>
