<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class matricula extends Model
{
  protected $table = 'matricula';       //definir la tabla al cual el modelo apunta

  //protected $primaryKey = 'id';       //definimos el id de la tabla NOTA: siempre de ser id autoincrement y llave primaria
  public $timestamps = false;         //que no busque campo timestamp en la db ya que larvel piensa que existen aqui los negamos ya no no existen en la tabla usuario

  protected $fillable = [
      'idalumno','idgrado','estado','idusuario','idano','num_talonario',
  ];
}
