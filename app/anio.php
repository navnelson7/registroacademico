<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class anio extends Model
{
  protected $table = 'ano';       //definir la tabla al cual el modelo apunta

  //protected $primaryKey = 'id';       //definimos el id de la tabla NOTA: siempre de ser id autoincrement y llave primaria

  protected $fillable = [
      'idano','ano','observacion',
  ];
}
