<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumno extends Model
{
  protected $table = 'alumno';       //definir la tabla al cual el modelo apunta
  public $timestamps = false;         //que no busque campo timestamp en la db ya que larvel piensa que existen aqui los negamos ya no no existen en la tabla usuario

  //protected $primaryKey = 'id';       //definimos el id de la tabla NOTA: siempre de ser id autoincrement y llave primaria

  protected $fillable = [
      'idalumno','nombres','apellidos','edad','sexo','fnacimiento','telefono',
      'padre','madre','encargado','parentesco','direccion','estado','idgrado',
  ];
}
