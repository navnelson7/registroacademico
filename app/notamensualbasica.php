<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notamensualbasica extends Model
{
  protected $table = 'notamensualbasica';       //definir la tabla al cual el modelo apunta

  //protected $primaryKey = 'id';       //definimos el id de la tabla NOTA: siempre de ser id autoincrement y llave primaria
  public $timestamps = false;

  protected $fillable = [
      'id','idalumno','idmae', 'idgrado', 'idasignatura', 'idmes', 'periodo', 'a1',
      'a2', 'a3', 'a4', 'promedio', 'p60', 'examen', 'p40', 'promes', 'idano',
  ];
}
