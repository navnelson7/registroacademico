<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuario';       //definir la tabla al cual el modelo apunta

    //protected $primaryKey = 'id';       //definimos el id de la tabla NOTA: siempre de ser id autoincrement y llave primaria

    public $timestamps = false;         //que no busque campo timestamp en la db ya que larvel piensa que existen aqui los negamos ya no no existen en la tabla usuario

    protected $fillable = [
        'idusuario','usuario','idmae','idgrado',
    ];

    protected $hidden = [
      'idmae'
    ];

    public function getAuthPassword()
    {
      return $this->idmae;
    }

}
