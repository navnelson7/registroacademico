<?php
/*
|--------------------------------------------------------------
|            myEloquentUserProvider
|--------------------------------------------------------------
|modificar metodo validateCredentials que se encarga de la
| comparacion de las credenciales en este caso omitiendo la encryptacion
|--------------------------------------------------------------
*/
namespace App;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;


class myEloquentUserProvider extends EloquentUserProvider
{

  public function validateCredentials(UserContract $user, array $credentials)
  {
      $plain = $credentials['password'];
      $hashed_value = $user->getAuthPassword();
      return $hashed_value == $plain;
  }

}
