<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notas extends Model
{
  protected $table = 'notas';       //definir la tabla al cual el modelo apunta

  //protected $primaryKey = 'id';       //definimos el id de la tabla NOTA: siempre de ser id autoincrement y llave primaria
  public $timestamps = false;

  protected $fillable = [
      'id', 'idalumno', 'idasignatura', 'idmae', 'idgrado', 'periodo', 'A1',
      'A2', 'A3', 'A4', 'A5', 'P50', 'EXAMEN', 'P20', 'EXAMENP', 'P30', 'PF',
      'ano', 'recup',
  ];
}
