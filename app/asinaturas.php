<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class asinaturas extends Model
{
  protected $table = 'asinaturas';       //definir la tabla al cual el modelo apunta

  //protected $primaryKey = 'id';       //definimos el id de la tabla NOTA: siempre de ser id autoincrement y llave primaria

  protected $fillable = [
    'id', 'idasignatura', 'asignatura', 'idgrado', 'idmae', 'estado',
  ];
}
