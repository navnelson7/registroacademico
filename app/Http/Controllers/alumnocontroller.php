<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\alumno;
use App\matricula;


class alumnocontroller extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function FormularioNuevoAlumno()
  {
    return view('NuevoAlumno');
  }
  //
  public function GuardarAlumno(Request $request)
  {
    $alumnos = new alumno();
    $alumnos->idalumno = $request->input('idalumno');
    $alumnos->nombres = $request->input('nombres');
    $alumnos->apellidos = $request->input('apellidos');
    $alumnos->edad = $request->input('edad');
    $alumnos->sexo = $request->input('sexo');
    $alumnos->fnacimiento = $request->input('fechadenacimiento');
    $alumnos->telefono = $request->input('telefono');
    $alumnos->padre = $request->input('padre');
    $alumnos->madre = $request->input('madre');
    $alumnos->encargado = $request->input('encargado');
    $alumnos->parentesco = $request->input('parentesco');
    $alumnos->direccion = $request->input('direccion');
    $alumnos->estado = $request->input('estado');
    $alumnos->idgrado = $request->input('idgrado');
    $alumnos->save();

    return redirect('/CBIS');
  }

}
