<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\matricula;
use App\grados;
use App\notas;
use App\alumno;
use App\mes;
use App\asinaturas;

class NotasBachilleratoController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * [FormularioNotas description]
   * @param Request $request [recibe parametros de la vista]
   */

  public function FormularioNotas(Request $request)
  {
    /**
     * |---------------------------|
     * |instancias de los modelos. |
     * |---------------------------|
     */
    $matriculas = matricula::all(); //array
    $grados = grados::all(); //array

    /**
     * |-------------------------------------------------------|
     * |variables que vienen vía post, para hacer una correcta |
     * |inserción de notas.                                    |
     * |-------------------------------------------------------|
     */
    $idalumno1 = $request->get('idalumno');//variable
    $idasignatura = $request->get('asignaturaseleccionada');//variable

    /**
     * |------------------------|
     * |inicializaciones.       |
     * |------------------------|
     */
    $idano = 0;
    $idalumno = 0;
    $idgrado = 0;

    foreach ($matriculas as $matricula) //se recorre el array matricula
    {
      for ($i=$matricula->idano; $i > $idano;)//seleccionar y traer el año en curso
      {
        $idano = $i;
      }
    }

    /**
     * |----------------------------------------------------------------|
     * |se recorre el array matricula nuevamente para hacer la consulta |
     * |puntual de los alumnos de bachillerato.                               |
     * |----------------------------------------------------------------|
     */

    foreach ($matriculas as $matricula)
    {
      if ($idalumno1 == $matricula->idalumno and $idano == $matricula->idano)
      {
        $idalumno = $matricula->idalumno;
        $idgrado = $matricula->idgrado;
        $idano = $matricula->idano;
      }
    }

    return view('MostrarAlumnoBachillerato')->with(compact('idalumno', 'idasignatura', 'idano','idgrado'));
  }

  /**
   * [GuardarNotas description]
   * @param Request $request [recibe parametros de la vista]
   * |--------------------------------------------|
   * |Método para inserción de notas.             |
   * |--------------------------------------------|
   */
  public function GuardarNotasBach(Request $request)
  {
    $idmae;
    $idgrado = $request->get('idgrado');
    $grados = grados::all();
    foreach ($grados as $grado)
    {
      if ($idgrado == $grado->idgrado)
      {
        $idmae = $grado->idmae;
      }
    }

    $notabachillerato = new notas();
    $notabachillerato->idalumno = $request->input('idalumno');
    $notabachillerato->idasignatura = $request->input('idasignatura');
    $notabachillerato->idmae = $idmae;
    $notabachillerato->idgrado = $request->input('idgrado');
    $notabachillerato->periodo = $request->input('periodo');
    $notabachillerato->A1 = $request->input('a1');
    $notabachillerato->A2 = $request->input('a2');
    $notabachillerato->A3 = $request->input('a3');
    $notabachillerato->A4 = $request->input('a4');
    $notabachillerato->A5 = $request->input('a5');
    $notabachillerato->P50 = $request->input('p50');
    $notabachillerato->EXAMEN = $request->input('exa');
    $notabachillerato->P20 = $request->input('p20');
    $notabachillerato->EXAMENP = $request->input('exap');
    $notabachillerato->P30 = $request->input('p30');
    $notabachillerato->PF = $request->input('pf');
    $notabachillerato->ano = $request->input('idano');
    $notabachillerato->recup = $request->input('recup');
    $notabachillerato->save();

    return redirect('/CBIS/SeleccionBachillerato');

  }

  /**
   * |------------------------------------------------|
   * |Métodos para filtrar información.               |
   * |------------------------------------------------|
   */
  public function FormularioBuscarAlumnoBach()
  {
    /**
     * |------------------------------------------------------|
     * |instancia obteniendo grados solo de bachilleratos.    |
     * |------------------------------------------------------|
     */
    $grados = grados::where('idgrado', '10')
                      ->orWhere('idgrado', '10B')
                      ->orWhere('idgrado', 'INC12')
                      ->orWhere('idgrado', '11')
                      ->orWhere('idgrado', '11A')
                      ->orWhere('idgrado', '11B')
                      ->get();

    $notas = notas::all();
    $valoresunicos = $notas->unique('ano');
    $valoresunicos->values()->all();

    $periodos = array("P1" => "PRIMER PERIODO",
                      "P2" => "SEGUNDO PERIODO",
                      "P3" => "TERCER PERIODO",
                      "P4" => "CUARTO PERIODO",
                );

    return view('BuscarAlumnoBach')->with(compact('grados', 'anios', 'periodos','notas', 'valoresunicos'));
  }


  /**
   * [ResultadoNotas description]
   * @param Request $request [description]
   */
  public function ResultadoNotasBach(Request $request)
  {

    $notas = notas::all();
    $matriculas = matricula::all();

    $idalumno1 = $request->get('idalumno');
    $periodo = $request->get('periodoseleccionado');
    $idgrado = $request->get('gradoseleccionado');
    $anio = $request->get('anioseleccionado');

    // $idasignatura = 0;
    // foreach ($notas as $nota)//recorrer tabla notas
    // {
    //   $idasignatura = $nota->idasignatura;
    // }
    //
    // $asignaturas = 0;
    // $asinaturas = asinaturas::all();
    // foreach ($asinaturas as $asinatura)//recorrer tabla grados y comparar
    // {
    //   if ($idasignatura == $asinatura->idasignatura)
    //   {
    //     $asignaturas = $asinatura->asignatura;
    //   }
    // }
    $asignaturas = asinaturas::all();


    $alumnos = alumno::all();
    $nombrecompleto = 0;
    foreach ($alumnos as $alumno)
    {
      if ($idalumno1 == $alumno->idalumno)
      {
        $nombrecompleto = $alumno->nombres . $alumno->apellidos;
      }
    }
    return view('MostrarNotasAlumnoBach')->with(compact('notas', 'idalumno1', 'periodo', 'idgrado', 'anio', 'nombrecompleto', 'asignaturas'));
  }

  /**
   * editar notas
   */
  public function FormularioEditarNotasBach($id)
  {

    $notas = notas::find($id);
    $alumnos = alumno::all();

    $nombrecompleto = 0;
    $alumnoid = 0;

    foreach ($alumnos as $alumno)
    {
      if ($notas->idalumno == $alumno->idalumno)
      {
        $nombrecompleto = $alumno->nombres . $alumno->apellidos;
      }
    }


    return view('EditarNotasAlumnoBach')->with(compact('notas','nombrecompleto'));
  }

  /**
   * guardar notas editadas
   */
   public function ActualizarNotasBach(Request $request, $id)
   {
     $nota1 = $request->input('a1');
     $nota2 = $request->input('a2');
     $nota3 = $request->input('a3');
     $nota4 = $request->input('a4');
     $nota5 = $request->input('a5');
     $examen = $request->input('examen');
     $examenp = $request->input('examenp');

     $P50 = (($nota1+$nota2+$nota3+$nota4+$nota5)/5)*0.5;
     $P20 = $examen*0.2;
     $P30 = $examenp*0.3;
     $PF = $P50+$P20+$P30;

     $nota = notas::find($id);
     $nota->A1 = $request->input('a1');
     $nota->A2 = $request->input('a2');
     $nota->A3 = $request->input('a3');
     $nota->A4 = $request->input('a4');
     $nota->A5 = $request->input('a5');
     $nota->P50 = $P50;
     $nota->P20 = $P20;
     $nota->P30 = $P30;
     $nota->PF = $PF;
     $nota->EXAMEN = $request->input('examen');
     $nota->EXAMENP = $request->input('examenp');
     $nota->save();

     return redirect('/CBIS/BuscarAlumno/Bachillerato');
   }

   public function EliminarNotasBach($id)
   {
     $notas = notas::find($id);
     $notas->delete();
     return redirect('/CBIS/BuscarAlumno/Bachillerato');
   }
}
