<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\asinaturas;

class SeleccionBachilleratoController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function SeleccionBachiller()
  {
  $asignaturas = asinaturas::all();

  return view('SeleccionBachillerato')->with(compact('asignaturas'));
  }
}
