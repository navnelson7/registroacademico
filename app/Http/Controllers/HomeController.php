<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\asignaturasbasica;
use App\mes;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $asignaturas = asignaturasbasica::all();
      $meses = mes::all();

      return view('home')->with(compact('asignaturas','meses'));
    }

}
