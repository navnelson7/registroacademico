<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\grados;
use App\matricula;

class GradosController extends Controller
{
    public function ListaGrados()
    {
      $grados = grados::all(); //array
      //$alumnos = alumno::all(); //array
      $matricula = matricula::all();
      $valoresunicos = $matricula->unique('idano');
      $valoresunicos->values()->all();
      return view('Matricula')->with(compact('grados','valoresunicos'));
    }
}
