<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\alumno;
use App\matricula;
use App\grados;

class MatriculaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ListarAlumnos(Request $request)
    {
      $idgrado = $request->input('gradoseleccionado');
      $idano = $request->input('idanoseleccionado');

      $alumnos = alumno::all(); //array
      $matriculas = matricula::whereIdgradoAndIdano($idgrado, $idano)
                              ->get();

      return view('ListaAlumnos')->with(compact('matriculas','alumnos'));
    }

    public function formulariomatricular(Request $request, $id)
    {
      $grados = grados::all(); //array
      $alumnos = alumno::all(); //array
      $matriculas = matricula::where('id', $id)
                              ->get();
      $matriculaanio = matricula::all(); //array

      $aniomayor = collect($matriculaanio)->max();

      return view('MatricuarAlumno')->with(compact('matriculas','alumnos','grados','aniomayor'));

    }

    public function GuardarMatricula(Request $request)
    {
      $matricula = new matricula();
      $matricula->idalumno = $request->input('idalumno');
      $matricula->idgrado = $request->input('gradoseleccionado');
      $matricula->estado = $request->input('estado');
      $matricula->idusuario = $request->input('idmae');
      $matricula->idano = $request->input('anio');
      $matricula->num_talonario = $request->input('talonario');

      $matricula->save();

      return redirect('/CBIS/matricula');
    }

}
