<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\alumno;
use App\grados;
use App\anio;
use App\matricula;
use App\notamensualbasica;
use App\mes;
use App\notas;
use App\asignaturasbasica;


class NotaMensualBasicaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     //metoo que sirve la vista para inragsar notas
    public function FormularioNotaMensualBasica(Request $request)
    {
      /**
       * |---------------------------|
       * |instancias de los modelos. |
       * |---------------------------|
       */
      $matriculas = matricula::all(); //array
      $grados = grados::all(); //array

      /**
       * |-------------------------------------------------------|
       * |variables que vienen vía post, para hacer una correcta |
       * |insercion de notas.                                    |
       * |-------------------------------------------------------|
       */
      $idalumno1 = $request->get('idalumno');//variable
      $idasignatura = $request->get('asignaturaseleccionada');//variable
      $idmes = $request->get('messeleccionado');//variable

      /**
       * |------------------------|
       * |inicializaciones.       |
       * |------------------------|
       */
      $idano = 0;
      $idalumno = 0;
      $idgrado = 0;
      //foreach para obtener el a;o correcto en curso
      foreach ($matriculas as $matricula) //se recorre el array matricula
      {
        for ($i=$matricula->idano; $i > $idano;)//seleccionar y traer el año en curso
        {
          $idano = $i;
        }
      }

      /**
       * |----------------------------------------------------------------|
       * |se recorre el array matricula nuevamente para hacer la consulta |
       * |puntual de los alumnos de basica.                               |
       * |----------------------------------------------------------------|
       */

      foreach ($matriculas as $matricula)
      {
        if ($idalumno1 == $matricula->idalumno and $idano == $matricula->idano and $matricula->idgrado <  10)
        {
          $idalumno = $matricula->idalumno;
          $idgrado = $matricula->idgrado;
          $idano = $matricula->idano;
        }
      }

      $alumnos = alumno::all(); //array
      $nombrecompleto = 0;
       //se recorre el array alumno para obtener el nmbre Completod
       //del almno seleccionado
      foreach ($alumnos as $alumno)
      {
        if ($idalumno1 == $alumno->idalumno)
        {
          $nombrecompleto = $alumno->nombres . $alumno->apellidos;
        }
      }

      return view('NotaMensualBasica')->with(compact('idasignatura', 'idmes', 'idano','idalumno','idgrado','nombrecompleto'));
    }
    //metodo que guardara los valores ingresados en la vista NotaMensualBasica
    public function GuardarNotas(Request $request)
    {
      $idmae;
      $idgrado = $request->get('idgrado');
      $grados = grados::all();
      //foreach para obtener el idemaestro correcto del grado al que
      //pertenese el alumno
      foreach ($grados as $grado)
      {
        if ($idgrado == $grado->idgrado)
        {
          $idmae = $grado->idmae;
        }
      }

      $notamensualbasica = new notamensualbasica();
      $notamensualbasica->idalumno = $request->input('idalumno');
      $notamensualbasica->idmae = $idmae;
      $notamensualbasica->idgrado = $request->input('idgrado');
      $notamensualbasica->idasignatura = $request->input('idasignatura');
      $notamensualbasica->idmes = $request->input('idmes');
      $notamensualbasica->periodo = $request->input('periodo');
      $notamensualbasica->a1 = $request->input('a1');
      $notamensualbasica->a2 = $request->input('a2');
      $notamensualbasica->a3 = $request->input('a3');
      $notamensualbasica->a4 = $request->input('a4');
      $notamensualbasica->promedio = $request->input('pro');
      $notamensualbasica->p60 = $request->input('p60');
      $notamensualbasica->examen = $request->input('exa');
      $notamensualbasica->p40 = $request->input('p40');
      $notamensualbasica->promes = $request->input('pf');
      $notamensualbasica->idano = $request->input('idano');
      $notamensualbasica->idalumno = $request->input('idalumno');
      $notamensualbasica->save();

      return redirect('/CBIS');
    }
    //sirve el formulario para realizar las busquedas y se injecta
    //la tabla meses para hacer la busqueda mas exacta
    public function FormularioBuscar()
    {
      $meses = mes::all();
      return view('Buscar')->with(compact('meses'));
    }
    //muestra el resultado de las notas que se an buscado y las muestra en una
    // tabla
    public function ResultadoNotas(Request $request)
    {
      $idalumno1 = $request->get('idalumno');//variable
      $idmes = $request->get('messeleccionado');//variable
      $notamensualbasica = notamensualbasica::all(); //array
      $matriculas = matricula::all(); //array
      $asignaturasbasica = asignaturasbasica::all();//array
      $idano = 0;
      foreach ($matriculas as $matricula) //se recorre el array matricula
      {
        for ($i=$matricula->idano; $i > $idano;)//seleccionar y traer el año en curso
        {
          $idano = $i;
        }
      }

      $alumnos = alumno::all(); //array
      $nombrecompleto = 0;
       //se recorre el array alumno para obtener el nmbre Completod
       //del almno seleccionado
      foreach ($alumnos as $alumno)
      {
        if ($idalumno1 == $alumno->idalumno)
        {
          $nombrecompleto = $alumno->nombres . $alumno->apellidos;
        }
      }
      return view('AlumnoNotas')->with(compact('notamensualbasica','idano','idalumno1','idmes','nombrecompleto', 'asignaturasbasica'));
    }
    /*
    |--------------------------------------------|
    |metodo que mostrar el formulario de editar  |
    |--------------------------------------------|
    */
    public function formularioeditardoNotas($id)
    {
      $notamensualbasica = notamensualbasica::find($id);

      return view('editaralunobasica')->with(compact('notamensualbasica'));
    }

    public function modificarNotas(Request $request, $id)
    {
      $notamensualbasica = notamensualbasica::find($id);
      $notamensualbasica->a1 = $request->input('a1');
      $notamensualbasica->a2 = $request->input('a2');
      $notamensualbasica->a3 = $request->input('a3');
      $notamensualbasica->a4 = $request->input('a4');
      $notamensualbasica->promedio = $request->input('pro');
      $notamensualbasica->examen = $request->input('exa');
      $notamensualbasica->save();

      return redirect('/CBIS/Buscar/');
    }


    public function EliminarAlumnoBasica($id)
    {
      $notamensualbasica = notamensualbasica::find($id);
      $notamensualbasica->delete();
      return redirect('/CBIS/Buscar/');
    }
  }
